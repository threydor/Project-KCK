# -*- coding: utf-8 -*-
from dialogs.print_hints import print_hints
from dialogs.window_methods import set_start_point
from unicurses import *


def select_platform_menu(list_to_show, is_next=False, start_from=0, model=None):
    def print_menu(menu_win, highlight):
        x = 2
        y = 1
        box(menu_win, 0, 0)
        mvwaddstr(menu_win, y, x, "Wybierz z listy:")
        y += 1
        for i in range(0, n_choices):
            if highlight == i + 1:
                wattron(menu_win, A_REVERSE)
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
                wattroff(menu_win, A_REVERSE)
            else:
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
            y += 1
        wrefresh(menu_win)

    p_list = update_view(list_to_show)
    p_list.sort()

    choices = []
    choices.extend(p_list[start_from:start_from + 7])

    if is_next:
        choices.append("<- Poprzednia czesc listy")
    if len(p_list) - start_from > 7:
        choices.append("Dalsza czesc listy ->")
    choices.append("Wyjdz bez wybierania...")

    n_choices = len(choices)
    keyboard_num_pressed = list(range(49, n_choices + 49))

    WIDTH = 60
    HEIGHT = 13

    highlight = 1
    choice = 0
    c = 0
    init_screen()
    start_color()
    init_pair(1, COLOR_YELLOW, COLOR_BLACK)  # used for the status bar

    startx, starty = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, starty, startx)
    keypad(menu_win, True)
    refresh()

    print_menu(menu_win, highlight)
    while True:
        print_hints("lub W aby wyjsc z widoku.")
        c = wgetch(menu_win)
        if c == KEY_UP:
            if highlight == 1:
                highlight = n_choices
            else:
                highlight -= 1

        elif c == KEY_DOWN:
            if highlight == n_choices:
                highlight = 1
            else:
                highlight += 1

        elif c == KEY_RIGHT:
            if len(p_list) - start_from > 7:
                select_platform_menu(p_list, is_next=True, start_from=start_from + 7, model=model)
                break
        elif c == KEY_LEFT:
            if is_next and (not len(p_list) - start_from > 7):
                select_platform_menu(p_list, is_next=False, start_from=start_from - 7, model=model)
                break

        elif c == 10:  # ENTER is pressed
            choice = highlight
            if (is_next and choice == 7) or \
                    (is_next and (not len(p_list) - start_from > 7) and (choice == n_choices - 1)):
                select_platform_menu(p_list, is_next=False, start_from=start_from - 7, model=model)
                break
            elif (len(p_list) - start_from > 7) and (choice == n_choices - 1):
                select_platform_menu(p_list, is_next=True, start_from=start_from + 7, model=model)
                break
            elif not (choice == n_choices or c == ord('q') or c == ord('w')):
                return choices[choice - 1]
            clrtoeol()
            refresh()

        elif c in keyboard_num_pressed:  # Shortcut is pressed
            choice = c - 48
            if (is_next and choice == 7) or \
                    (is_next and (not len(p_list) - start_from > 7) and (choice == n_choices - 1)):
                select_platform_menu(p_list, is_next=False, start_from=start_from - 7, model=model)
                break
            elif (len(p_list) - start_from > 7) and (choice == n_choices - 1):
                select_platform_menu(p_list, is_next=True, start_from=start_from + 7, model=model)
                break
            elif not (choice == n_choices or c == ord('q') or c == ord('w')):
                return choices[choice - 1]
            clrtoeol()
            refresh()

        else:
            clrtoeol()
            refresh()

        print_menu(menu_win, highlight)

        if choice == n_choices or c == ord('q') or c == ord('w'):
            init_screen()
            break


def update_current_view(is_next, model, start_from):
    select_platform_menu(model.list_items(), is_next, start_from, model)


def update_view(list_to_show):
    p_list = list_to_show
    return p_list


def init_screen():
    clear()
    noecho()
    cbreak()
    curs_set(0)
