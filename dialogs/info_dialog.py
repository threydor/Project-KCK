# -*- coding: utf-8 -*-
from unicurses import *
from dialogs.window_methods import set_start_point


def show_info_dialog(text):
    info = [text, "", "OK" + 10 * " " + "Wcisnij dowolny klawisz..."]
    n_info = len(info)
    HEIGHT, WIDTH = set_dimensions(info)

    def print_info(window):
        x = 2
        y = 2
        for i in range(0, n_info):
            box(window, 0, 0)
            mvwaddstr(window, y, x, info[i])
            y += 1
        wrefresh(window)

    clear()
    noecho()
    cbreak()
    start_x, start_y = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    print_info(menu_win)
    wgetch(menu_win)
    refresh()


def set_dimensions(info):
    WIDTH = len(max(info, key=len)) + 7
    HEIGHT = len(info) + 4
    return HEIGHT, WIDTH
