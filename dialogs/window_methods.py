from unicurses import wborder, CCHAR, wrefresh, delwin, echo, box, mvaddstr, refresh, mvgetstr


def destroy_win(local_win):
    wborder(local_win, CCHAR(' '), CCHAR(' '), CCHAR(' '), CCHAR(' '), CCHAR(' '), CCHAR(' '), CCHAR(' '), CCHAR(' '))
    wrefresh(local_win)
    delwin(local_win)


def my_raw_input(window, r, c, prompt_string):
    echo()
    box(window, 0, 0)
    mvaddstr(r, c, prompt_string)
    refresh()
    input = mvgetstr(r + 1, c)
    return input


def set_start_point(HEIGHT, WIDTH):
    startx = int((80 - WIDTH) / 2)
    starty = int((30 - HEIGHT) / 2)
    return startx, starty
