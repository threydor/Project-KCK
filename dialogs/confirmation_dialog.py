# -*- coding: utf-8 -*-
from dialogs.info_dialog import *
from list_views.list_view import *


def show_confirmation(custom_message):
    def print_menu(menu_win, highlight):
        x = 2
        y = 1
        text = custom_message
        box(menu_win, 0, 0)
        mvwaddstr(menu_win, y, x, text)
        y += 1
        x += 2
        for i in range(0, n_choices):
            if highlight == i + 1:
                wattron(menu_win, A_REVERSE)
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
                wattroff(menu_win, A_REVERSE)
            else:
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
            y += 1
        wrefresh(menu_win)

    choices, choices_bool = set_menu_options()

    n_choices = len(choices)

    # Defining num shorctus
    keyboard_num_pressed = list(range(49, n_choices + 49))

    HEIGHT, WIDTH = set_dimensions(choices)
    highlight = 2
    choice = 0
    init_screen()
    startx, starty = set_start_point(HEIGHT, WIDTH)
    menu_win = newwin(HEIGHT, WIDTH, starty, startx)
    keypad(menu_win, True)
    refresh()
    print_menu(menu_win, highlight)

    while True:
        print_hints("lub W aby anulowac.")
        c = wgetch(menu_win)
        if c == KEY_UP:
            if highlight == 1:
                highlight = n_choices
            else:
                highlight -= 1
        elif c == KEY_DOWN:
            if highlight == n_choices:
                highlight = 1
            else:
                highlight += 1
        elif c == 10:  # ENTER is pressed
            choice = highlight
            if not (choice == n_choices or c == ord('q') or c == ord('w')):
                return choices_bool[choice - 1]
        elif c in keyboard_num_pressed:
            choice = c - 48
            if not (choice == n_choices or c == ord('q') or c == ord('w')):
                return choices_bool[choice - 1]
        else:
            move(22, 0)
            clrtoeol()
            refresh()
        print_menu(menu_win, highlight)
        if choice == n_choices or c == ord('q') or c == ord('w'):
            init_screen()
            break
    refresh()
    wclear(menu_win)
    endwin()


def set_menu_options():
    choices = ["Tak", "Nie", "Anuluj"]
    choices_bool = [True, False]
    return choices, choices_bool


def set_dimensions(choices):
    WIDTH = 40
    HEIGHT = len(choices) + 4
    return HEIGHT, WIDTH


def init_screen():
    clear()
    noecho()
    cbreak()
    curs_set(0)
