from unicurses import mvaddstr
from unicurses import *


def print_hints(text):
    mvaddstr(2, 2, "Uzyj strzalek aby poruszac sie po menu, wcisnij ")
    attron(COLOR_PAIR(1))
    addstr("ENTER")
    attroff(COLOR_PAIR(1))
    addstr(" aby potwierdzic.")

    mvaddstr(3, 2, "Dostepne sa rowniez skroty klawiaturowe: klawisz z cyfra odpowiada wybraniu")
    clrtoeol()
    mvaddstr(4, 2, "opcji o tym samym numerze.")
    clrtoeol()
    mvaddstr(23, 2, "Wcisnij")
    clrtoeol()
    attron(COLOR_PAIR(1))
    addstr(" Q ")
    attroff(COLOR_PAIR(1))
    mvaddstr(23, 12, text)
    clrtoeol()
    refresh()