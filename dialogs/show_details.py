from dialogs.info_dialog import show_info_dialog
from dialogs.window_methods import set_start_point
from models import publisher
import models.publisher as publishers
from models import platform
import models.platform as platforms
from models import game
import models.game as games
from unicurses import *


def publisher_details(name):
    publisher = publishers.publisher_list[name]
    info = []
    info.extend(["Nazwa:", str(publisher['_name']),
                 "OK"])
    HEIGHT, WIDTH = set_dimensions(info)

    def print_info(window):
        x = 2
        y = 2
        for i in range(0, len(info) - 1):
            box(window, 0, 0)
            if i % 2 == 0:
                attron(A_BOLD)
                mvwaddstr(window, y, x, info[i])
                attroff(A_BOLD)
            else:
                mvwaddstr(window, y, x + 2, info[i])
            y += 1
        mvwaddstr(window, y + 1, x + 3, "OK")
        wrefresh(window)

    clear()
    noecho()
    cbreak()
    start_x, start_y = set_start_point(HEIGHT, WIDTH - 2)
    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    print_info(menu_win)
    wgetch(menu_win)
    refresh()


def platform_details(name):
    platform = platforms.platform_list[name]
    info = []
    info.extend(["Nazwa:", str(platform['_name']),
                 "OK"])
    HEIGHT, WIDTH = set_dimensions(info)

    def print_info(window):
        x = 2
        y = 2
        for i in range(0, len(info) - 1):
            box(window, 0, 0)
            if i % 2 == 0:
                attron(A_BOLD)
                mvwaddstr(window, y, x, info[i])
                attroff(A_BOLD)
            else:
                mvwaddstr(window, y, x + 2, info[i])
            y += 1
        mvwaddstr(window, y + 1, x + 3, "OK")
        wrefresh(window)

    clear()
    noecho()
    cbreak()
    start_x, start_y = set_start_point(HEIGHT, WIDTH - 2)
    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    print_info(menu_win)
    wgetch(menu_win)
    refresh()


def game_details(name):
    game = games.game_list[name]
    info = []
    info.extend(["Nazwa:", str(game['_name']),
                 "Wydawca:", str(game['_publisher']),
                 "Platforma:", str(game['_platform']),
                 "Kod kreskowy:", str(game['_barcode']),
                 "Dostepna ilosc:", str(game['_stock']),
                 "Kategoria:", str(game['_category']),
                 "OK"])
    HEIGHT, WIDTH = set_dimensions(info)

    def print_info(window):
        x = 2
        y = 2
        for i in range(0, len(info) - 1):
            box(window, 0, 0)
            if i % 2 == 0:
                attron(A_BOLD)
                mvwaddstr(window, y, x, info[i])
                attroff(A_BOLD)
            else:
                mvwaddstr(window, y, x + 2, info[i])
            y += 1
        mvwaddstr(window, y+1, x + 10, "OK")
        wrefresh(window)

    clear()
    noecho()
    cbreak()
    start_x, start_y = set_start_point(HEIGHT, WIDTH-2)
    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    print_info(menu_win)
    wgetch(menu_win)
    refresh()


def show_item_details(name, model):
    if model == publisher:
        publisher_details(name)
    elif model == platform:
        platform_details(name)
    elif model == game:
        game_details(name)
    else:
        show_info_dialog("Problemy.")


def set_dimensions(info):
    WIDTH = len(max(info, key=len)) + 7
    HEIGHT = len(info) + 4
    return HEIGHT, WIDTH
