# -*- coding: utf-8 -*-
from time import strftime

import models.game as games
import models.platform as platforms
import models.publisher as publishers
from dialogs.info_dialog import *
from dialogs.window_methods import my_raw_input
from list_views.select_item_list_view import select_platform_menu
from models.game import *
from models.platform import list_items as list_platforms
from models.publisher import list_items as list_publishers

def show_add_game_dialog():
    text = "Wprowadz nazwe nowej gry: "
    HEIGHT, WIDTH = set_dimensions(text)

    init_screen()
    start_x, start_y = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    name = my_raw_input(menu_win, 5, 20, text)
    barcode = my_raw_input(menu_win, 7, 20, "Podaj kod kreskowy gry:")
    price = my_raw_input(menu_win, 9, 20, "Podaj cene gry:")
    category = my_raw_input(menu_win, 11, 20, "Podaj kategorie gry:")
    stock = my_raw_input(menu_win, 13, 20, "Podaj stan magazynowy gry:")
    platform = select_platform_menu(list_platforms(), is_next=False, start_from=0, model=platforms)
    publisher = select_platform_menu(list_publishers(), is_next=False, start_from=0, model=publishers)
    clear()

    if len(name) > 3:
        show_info_dialog("Dodano nowa gre o nazwie: " + name)
        games.add_item(Game(barcode, name, price, strftime("%Y-%m-%d %H:%M:%S"), platform, publisher, category, stock=stock))
    else:
        show_info_dialog("Podana nazwa jest za krotka, wymagane minimum 3 znaki.")

    refresh()


def set_start_point(HEIGHT, WIDTH):
    start_x = int((80 - WIDTH) / 2)
    start_y = int((24 - HEIGHT) / 2)
    return start_x, start_y


def set_dimensions(info):
    WIDTH = len(max(info, key=len)) + 30
    HEIGHT = len(info) + 4
    return HEIGHT, WIDTH


def init_screen():
    clear()
    noecho()
    cbreak()
