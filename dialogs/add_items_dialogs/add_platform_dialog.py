# -*- coding: utf-8 -*-
from dialogs.info_dialog import *
import models.platform as platforms
from models.platform import *
from dialogs.window_methods import my_raw_input


def show_add_platform_dialog():
    text = "Wprowadz nazwe nowej platformy: "
    HEIGHT, WIDTH = set_dimensions(text)

    init_screen()
    start_x, start_y = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    name = my_raw_input(menu_win, 5, 20, text)
    clear()

    if len(name) > 3:
        show_info_dialog("Dodano nowa platforme o nazwie: " + name)
        platforms.add_item(Platform(name))
    else:
        show_info_dialog("Podana nazwa jest za krotka, wymagane minimum 3 znaki.")

    refresh()


def set_start_point(HEIGHT, WIDTH):
    start_x = int((80 - WIDTH) / 2)
    start_y = int((24 - HEIGHT) / 2)
    return start_x, start_y


def set_dimensions(info):
    WIDTH = len(max(info, key=len)) + 30
    HEIGHT = len(info) + 4
    return HEIGHT, WIDTH


def init_screen():
    clear()
    noecho()
    cbreak()
