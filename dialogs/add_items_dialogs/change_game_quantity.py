# -*- coding: utf-8 -*-
import models.publisher as publishers
from dialogs.info_dialog import *
from dialogs.window_methods import my_raw_input
from models.publisher import *


def show_change_quantity(name):
    import models.game as games
    game = games.game_list[name]
    text = "Wprowadz nowa ilosc: "
    HEIGHT, WIDTH = set_dimensions(text)

    init_screen()
    start_x, start_y = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    quantity = my_raw_input(menu_win, 5, 20, text)
    clear()
    show_info_dialog("Nowa ilosc = " + quantity)
    game["_stock"] = quantity
    refresh()


def set_dimensions(info):
    WIDTH = len(max(info, key=len)) + 30
    HEIGHT = len(info) + 4
    return HEIGHT, WIDTH


def set_start_point(HEIGHT, WIDTH):
    start_x = int((80 - WIDTH) / 2)
    start_y = int((24 - HEIGHT) / 2)
    return start_x, start_y


def init_screen():
    clear()
    noecho()
    cbreak()
