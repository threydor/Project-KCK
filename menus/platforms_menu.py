# -*- coding: utf-8 -*-
import models.platform as platforms
from dialogs.add_items_dialogs.add_platform_dialog import show_add_platform_dialog
from dialogs.info_dialog import *
from dialogs.window_methods import my_raw_input
from dialogs.window_methods import set_start_point
from list_views.list_view import *


def show_menu():
    def print_menu(menu_win, highlight):
        x = 1
        y = 1
        box(menu_win, 0, 0)
        wattron(menu_win, A_BOLD)
        mvwaddstr(menu_win, y, x, "Menu platform:")
        wattroff(menu_win, A_BOLD)
        y += 1
        x += 1
        for i in range(0, n_choices):
            if highlight == i + 1:
                wattron(menu_win, A_REVERSE)
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
                wattroff(menu_win, A_REVERSE)
            else:
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
            y += 1
        wrefresh(menu_win)

    choices, methods_with_parameters, methods_without_parameters = set_menu_options()
    n_choices = len(choices)

    # Definining num shortcuts
    keyboard_num_pressed = list(range(49, n_choices + 49))
    HEIGHT, WIDTH = set_dimensions(choices)
    highlight = 1
    choice = 0
    init_screen()
    startx, starty = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, starty, startx)
    keypad(menu_win, True)
    refresh()
    print_menu(menu_win, highlight)

    while True:
        print_hints("cofnac.")
        c = wgetch(menu_win)
        if c == KEY_UP:
            if highlight == 1:
                highlight = n_choices
            else:
                highlight -= 1
        elif c == KEY_DOWN:
            if highlight == n_choices:
                highlight = 1
            else:
                highlight += 1
        elif c == 10:  # ENTER is pressed
            choice = highlight
            handle_input(choice, menu_win, methods_with_parameters, methods_without_parameters)
        elif c in keyboard_num_pressed:
            choice = c - 48
            handle_input(choice, menu_win, methods_with_parameters, methods_without_parameters)
        else:
            move(22, 0)
            clrtoeol()
            refresh()
        print_menu(menu_win, highlight)
        if choice == n_choices or c == ord('q') or c == ord('w'):
            break
    refresh()
    endwin()


def handle_input(choice, menu_win, methods_with_parameters, methods_without_parameters):
    if choice == 1:
        show_list(platforms.list_items(), model=platforms)
    elif choice == 2:
        clear()
        refresh()
        text_searched = my_raw_input(menu_win, 5, 20, "Podaj fragment nazwy platformy do wyszukania:")
        result = [x for x in platforms.list_items() if text_searched.lower() in x.lower()]
        if result:
            show_list(result, model=platforms)
        else:
            show_info_dialog("Nie znaleziono zadnych pasujacych wynikow!")
    else:
        execute_method(choice, methods_without_parameters)
        execute_method_with_dialog(choice, methods_with_parameters)
    clrtoeol()
    refresh()


def set_dimensions(choices):
    WIDTH = len(max(choices, key=len)) + 7
    HEIGHT = len(choices) + 4
    return HEIGHT, WIDTH


def set_menu_options():
    choices = ["Pokaz liste platform", "Szukaj platformy", "Dodaj platforme", "Eksport do pliku JSON",
               "Import z pliku JSON", "Cofnij..."]
    methods_without_parameters = {3: show_add_platform_dialog}
    methods_with_parameters = {4: platforms.save_to_json, 5: platforms.load_from_json}
    return choices, methods_with_parameters, methods_without_parameters


def init_screen():
    clear()
    noecho()
    cbreak()
    curs_set(0)


def execute_method_with_dialog(choice, choice_method_with_dialog):
    if choice in choice_method_with_dialog:
        text = choice_method_with_dialog[choice]()
        show_info_dialog(text)


def execute_method(choice, choice_method):
    if choice in choice_method:
        choice_method[choice]()
