# -*- coding: utf-8 -*-
import menus.games_menu as games_menu
import menus.platforms_menu as platforms_menu
import menus.publishers_menu as publishers_menu
import models.game as games
import models.platform as platforms
import models.publisher as publishers
from dialogs.confirmation_dialog import show_confirmation
from dialogs.info_dialog import *
from dialogs.print_hints import print_hints
from dialogs.window_methods import set_start_point


def show_menu():
    def print_menu(menu_win, highlight):
        x = 7
        y = 2
        box(menu_win, 0, 0)
        wattron(menu_win, A_BOLD)
        mvwaddstr(menu_win, y, x, "Menu glowne:")
        wattroff(menu_win, A_BOLD)
        y += 1
        x += 1
        for i in range(0, n_choices):
            if highlight == i + 1:
                wattron(menu_win, A_REVERSE)
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
                wattroff(menu_win, A_REVERSE)
            else:
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
            y += 1
        wrefresh(menu_win)

    choices, methods_list = set_menu_options()
    n_choices = len(choices)
    keyboard_num_pressed = list(range(49, n_choices + 49))

    WIDTH = len(max(choices, key=len)) + 20
    HEIGHT = len(choices) + 6

    highlight = 1
    choice = 0
    c = 0
    stdscr = initscr()
    init_screen()
    start_color()
    init_pair(1, COLOR_YELLOW, COLOR_BLACK)  # used for the status bar
    startx, starty = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, starty, startx)
    show_info_dialog("Ustawienia zaladowane pomyslnie. Witaj z powrotem!")

    keypad(menu_win, True)
    refresh()
    print_menu(menu_win, highlight)

    while True:
        print_hints("aby wyjsc.")
        c = wgetch(menu_win)
        if c == KEY_UP:
            if highlight == 1:
                highlight = n_choices
            else:
                highlight -= 1

        elif c == KEY_DOWN:
            if highlight == n_choices:
                highlight = 1
            else:
                highlight += 1

        elif c == 10:  # ENTER is pressed
            choice = highlight
            execute_method(choice, methods_list)
            refresh()

        elif c in keyboard_num_pressed:  # Shortcut is pressed
            choice = c - 48
            execute_method(choice, methods_list)
            refresh()

        else:
            move(22, 0)
            refresh()
        print_menu(menu_win, highlight)
        if choice == n_choices or c == ord('q') or c == KEY_EXIT:
            break

    refresh()
    try:
        if show_confirmation("Czy zapisac zmiany przed wyjsciem?"):
            games.save_to_json()
            publishers.save_to_json()
            platforms.save_to_json()
    finally:
        clear()
        endwin()


def set_menu_options():
    choices = ["Menu gier", "Menu wydawcow", "Menu platform", "Wyjdz..."]
    methods_list = {1: games_menu.show_menu, 2: publishers_menu.show_menu, 3: platforms_menu.show_menu}
    return choices, methods_list


def init_screen():
    clear()
    noecho()
    cbreak()
    curs_set(0)


def execute_method(choice, choice_method):
    if choice in choice_method:
        choice_method[choice]()
