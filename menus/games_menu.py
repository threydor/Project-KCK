# -*- coding: utf-8 -*-
import models.game as games
from dialogs.add_items_dialogs.add_game_dialog import show_add_game_dialog
from dialogs.info_dialog import *
from dialogs.print_hints import print_hints
from dialogs.window_methods import my_raw_input
from dialogs.window_methods import set_start_point
from list_views.list_view import show_list


def show_menu():
    def print_menu(menu_win, highlight):
        x = 1
        y = 1
        box(menu_win, 0, 0)
        wattron(menu_win, A_BOLD)
        mvwaddstr(menu_win, y, x, "Menu gier:")
        wattroff(menu_win, A_BOLD)
        y += 1
        x += 1
        for i in range(0, n_choices):
            if highlight == i + 1:
                wattron(menu_win, A_REVERSE)
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
                wattroff(menu_win, A_REVERSE)
            else:
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
            y += 1
        wrefresh(menu_win)

    choice_method_no_dialog, choice_method_with_dialog, choices = set_menu_options()
    n_choices = len(choices)

    # Definining num shortcuts
    keyboard_num_pressed = list(range(49, n_choices + 49))
    HEIGHT, WIDTH = set_dimensions(choices)
    highlight = 1
    choice = 0
    init_screen()
    start_x, start_y = set_start_point(HEIGHT, WIDTH)

    menu_win = newwin(HEIGHT, WIDTH, start_y, start_x)
    keypad(menu_win, True)
    refresh()
    print_menu(menu_win, highlight)

    while True:
        print_hints("lub W aby cofnac.")
        c = wgetch(menu_win)
        if c == KEY_UP:
            if highlight == 1:
                highlight = n_choices
            else:
                highlight -= 1
        elif c == KEY_DOWN:
            if highlight == n_choices:
                highlight = 1
            else:
                highlight += 1
        elif c == 10:  # ENTER is pressed
            choice = highlight
            handle_input(choice, choice_method_no_dialog, choice_method_with_dialog, menu_win)
        elif c in keyboard_num_pressed:
            choice = c - 48
            handle_input(choice, choice_method_no_dialog, choice_method_with_dialog, menu_win)
        else:
            move(22, 0)
            clrtoeol()
            refresh()
        print_menu(menu_win, highlight)
        if choice == n_choices or c == ord('q') or c == ord('w'):
            init_screen()
            break
    refresh()
    endwin()


def handle_input(choice, choice_method_no_dialog, choice_method_with_dialog, menu_win):
    if choice == 1:
        show_list(games.list_items(), model=games)
    elif choice == 2:
        clear()
        refresh()
        text_searched = my_raw_input(menu_win, 5, 20, "Podaj fragment nazwy gry do wyszukania:")
        result = [x for x in games.list_items() if text_searched.lower() in x.lower()]
        if result:
            show_list(result, model=games)
        else:
            show_info_dialog("Nie znaleziono zadnych pasujacych wynikow!")
    else:
        execute_method(choice, choice_method_no_dialog)
        execute_method_with_dialog(choice, choice_method_with_dialog)
    clrtoeol()
    refresh()


def set_dimensions(choices):
    WIDTH = len(max(choices, key=len)) + 7
    HEIGHT = len(choices) + 4
    return HEIGHT, WIDTH


def set_menu_options():
    choices = ["Wyswietl liste gier", "Szukaj gry", "Dodaj gre", "Eksport do pliku JSON",
               "Import z pliku JSON", "Cofnij..."]
    choice_method_no_dialog = {3: show_add_game_dialog}
    choice_method_with_dialog = {4: games.save_to_json, 5: games.load_from_json}
    return choice_method_no_dialog, choice_method_with_dialog, choices


def init_screen():
    clear()
    noecho()
    cbreak()
    curs_set(0)


def execute_method_with_dialog(choice, choice_method_with_dialog):
    if choice in choice_method_with_dialog:
        text = choice_method_with_dialog[choice]()
        show_info_dialog(text)


def execute_method(choice, choice_method):
    if choice in choice_method:
        choice_method[choice]()
