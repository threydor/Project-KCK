# -*- coding: utf-8 -*-
import models.game as games
from dialogs.confirmation_dialog import show_confirmation
from dialogs.info_dialog import *
from dialogs.print_hints import print_hints
from dialogs.show_details import show_item_details
from dialogs.window_methods import set_start_point
from models import platform as platforms
from models import publisher as publishers


def show_selected_dialog(name, model):
    def print_menu(menu_win, highlight):
        x = 2
        y = 1
        box(menu_win, 0, 0)
        mvwaddstr(menu_win, y, x, name + ":")
        y += 1
        for i in range(0, n_choices):
            if highlight == i + 1:
                wattron(menu_win, A_REVERSE)
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
                wattroff(menu_win, A_REVERSE)
            else:
                mvwaddstr(menu_win, y, x, str(i + 1) + ". " + choices[i])
            y += 1
        wrefresh(menu_win)

    choices, method_with_parameters, methods_without_parameters = set_menu_options(model)

    n_choices = len(choices)

    # Defining num shorctus
    keyboard_num_pressed = list(range(49, n_choices + 49))

    HEIGHT, WIDTH = set_dimensions(name, choices)
    highlight = 1
    choice = 0
    init_screen()
    startx, starty = set_start_point(HEIGHT, WIDTH)
    menu_win = newwin(HEIGHT, WIDTH, starty, startx)
    keypad(menu_win, True)
    refresh()
    print_menu(menu_win, highlight)

    while True:
        print_hints("lub W aby anulowac.")
        c = wgetch(menu_win)
        if c == KEY_UP:
            if highlight == 1:
                highlight = n_choices
            else:
                highlight -= 1
        elif c == KEY_DOWN:
            if highlight == n_choices:
                highlight = 1
            else:
                highlight += 1
        elif c == 10:  # ENTER is pressed
            choice = highlight
            from list_views.list_view import show_list
            if choice == 1:
                show_item_details(name, model=model)
            elif choice == 2 and (model == publishers):
                show_list(publishers.get_game_list(name), model=games)
            elif choice == 2 and (model == platforms):
                show_list(platforms.get_game_list(name), model=games)
            elif choice == 2 and (model == games):
                from dialogs.add_items_dialogs.change_game_quantity import show_change_quantity
                show_change_quantity(name)
            elif choice == 3:
                show_info_dialog("Mozliwosc edycji nie zostala zaimplementowana.")
            else:
                execute_method(choice, methods_without_parameters)
                execute_method_with_dialog(choice, method_with_parameters, name)
                break
        elif c in keyboard_num_pressed:
            choice = c - 48
            from list_views.list_view import show_list
            if choice == 1:
                show_item_details(name, model=model)
            elif choice == 2 and (model == publishers):
                show_list(publishers.get_game_list(name), model=games)
            elif choice == 2 and (model == platforms):
                show_list(platforms.get_game_list(name), model=games)
            elif choice == 2 and (model == games):
                from dialogs.add_items_dialogs.change_game_quantity import show_change_quantity
                show_change_quantity(name)
            elif choice == 3:
                show_info_dialog("Mozliwosc edycji nie zostala zaimplementowana.")
            else:
                execute_method(choice, methods_without_parameters)
                execute_method_with_dialog(choice, method_with_parameters, name)
                break
        else:
            move(22, 0)
            clrtoeol()
            refresh()
        print_menu(menu_win, highlight)
        if choice == n_choices or c == ord('q') or c == ord('w'):
            break
    refresh()
    wclear(menu_win)
    endwin()


def set_menu_options(model):
    if model == publishers:
        choices = ["Szczegoly", "Lista gier", "Edytuj", "Usun", "Anuluj"]
        methods_without_parameters = {}
        method_with_parameters = {4: model.remove_item}
    elif model == platforms:
        choices = ["Szczegoly", "Lista gier", "Edytuj", "Usun", "Anuluj"]
        methods_without_parameters = {}
        method_with_parameters = {4: model.remove_item}
    else:
        choices = ["Szczegoly", "Zmien ilosc","Edytuj", "Usun", "Anuluj"]
        methods_without_parameters = {}
        method_with_parameters = {4: model.remove_item}
    return choices, method_with_parameters, methods_without_parameters


def set_dimensions(name, choices):
    WIDTH = max(len(name), len(max(choices, key=len))) + 7
    HEIGHT = len(choices) + 4
    return HEIGHT, WIDTH


def init_screen():
    clear()
    noecho()
    cbreak()
    curs_set(0)


def execute_method_with_dialog(choice, choice_method_with_dialog, name):
    if choice in choice_method_with_dialog:
        try:
            if show_confirmation("Czy potwierdzasz akcje?"):
                text = choice_method_with_dialog[choice](name)
                show_info_dialog(text)
        except:
            show_info_dialog("Nie udalo sie usunac obiektu")


def execute_method(choice, choice_method):
    if choice in choice_method:
        choice_method[choice]()
