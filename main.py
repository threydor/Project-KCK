import models.game as games
import models.platform as platforms
import models.publisher as publishers
from menus import main_menu

menu_items = ["1. Produkty", "2. Wydawcy", "3. Platformy", "4. Opcje", "5. Wyjdz"]

try:
    print(publishers.load_from_json())
    print(publishers.list_items())
    print(platforms.load_from_json())
    print(platforms.list_items())
    print(games.load_from_json())
    print(games.list_items())
except KeyError:
    pass


main_menu.show_menu()

