import json
from json import JSONEncoder
import models.game as games

platform_list = {}


class Platform(object):
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value


def add_item(platform):
    if platform.name not in platform_list:
        platform_list[platform.name] = platform
        save_to_json()
        load_from_json()
        return "Successfully added " + platform.name + " to the platform list!"
    else:
        return "Platform " + platform.name + " is already on the list!"


def remove_item(key):
    try:
        platform_list.pop(key, None)
        return "Success"
    except KeyError:
        return "Not on platform list."


def list_items():
    return list(platform_list)


class PlatformEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def save_to_json():
    with open('platforms.json', 'w+') as platforms_json_file:
        json.dump(platform_list, platforms_json_file, cls=PlatformEncoder, sort_keys=True)
    return "Zapis do pliku JSON zakonczony powodzeniem!"


def load_from_json():
    try:
        with open('platforms.json', 'r') as platforms_json_file:
            data = json.load(platforms_json_file)
        for key, item in data.items():
            platform_list[key] = item
    except FileNotFoundError:
        return "Nie znaleziono pliku  platforms.json."
    return "Odczyt z pliku JSON zakonczony powodzeniem!"


def get_game_list(name):
    game_list = []
    for keys, values in games.game_list.items():
        if values["_platform"] == name:
            game_list.append(values["_name"])
    return game_list
