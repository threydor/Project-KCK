from json import JSONEncoder
import json

game_list = {}


class Game(object):
    id = 0

    def __init__(self, barcode, name, price, release_date, platform, publisher, category, stock=0):
        self._barcode = barcode
        self._name = name
        self._price = price
        self._release_date = release_date
        self._platform = platform
        self._publisher = publisher
        self._category = category
        self._stock = stock

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def barcode(self):
        return self._barcode

    @barcode.setter
    def barcode(self, value):
        if value >= 0:
            self._barcode = value

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, value):
        self._platform = value

    @property
    def publisher(self):
        return self._publisher

    @publisher.setter
    def publisher(self, value):
        self._publisher = value

    @property
    def category(self):
        return self._category

    @category.setter
    def category(self, value):
        self._category = value

    @property
    def stock(self):
        return self._stock

    @stock.setter
    def stock(self, value):
        if value >= 0:
            self._stock = value


def list_items():
    return list(game_list.keys())


def add_item(game):
    if game.name not in game_list:
        game_list[game.name] = game
        save_to_json()
        load_from_json()
        return "Successfully added " + game.name + " to the game list!"
    else:
        return "Platform " + game.name + " is already on the list!"


def remove_item(key):
    try:
        game_list.pop(key, None)
        return "Successfully removed game: " + key
    except KeyError:
        return "Game " + key + " is not on the game list."


class GameEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def save_to_json():
    with open('games.json', 'w+') as games_json_file:
        json.dump(game_list, games_json_file, cls=GameEncoder, sort_keys=True)
    return "Zapis do pliku JSON zakonczony powodzeniem!"


def load_from_json():
    try:
        with open('games.json', 'r') as games_json_file:
            data = json.load(games_json_file)
        for key, item in data.items():
            game_list[key] = item
    except FileNotFoundError:
        return "Nie znaleziono pliku platforms.json."
    return "Odczyt z pliku JSON zakonczony powodzeniem!"
