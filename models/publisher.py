from json import JSONEncoder
import json

publisher_list = {}


# Publisher class, easly moddable.
class Publisher(object):
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value


# Adding a publisher to publisher list dict
def add_item(publisher):
    if publisher.name not in publisher_list:
        publisher_list[publisher.name] = publisher
        save_to_json()
        load_from_json()
        return "Successfully added publisher: " + publisher.name + "."
    else:
        return "Publisher " + publisher.name + " is already on the list."


# Removing publisher from a list
def remove_item(key):
    try:
        publisher_list.pop(key, None)
        return "Successfully removed publisher: " + key
    except KeyError:
        return "Publisher " + key + " is not on the publisher list."


# Return list of publishers.
def list_items():
    return list(publisher_list.keys())


# Custom encoder class for JSON
class PublisherEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


# Encoding publisher list to JSON file
def save_to_json():
    with open('publishers.json', 'w+') as publisher_json_file:
        json.dump(publisher_list, publisher_json_file, cls=PublisherEncoder, sort_keys=True)
    return "Zapis do pliku JSON zakonczony powodzeniem!"


# Loading and decoding publisher list JSON file.
def load_from_json():
    try:
        with open('publishers.json', 'r') as publisher_json_file:
            data = json.load(publisher_json_file)
        for key, item in data.items():
            publisher_list[key] = item
    except FileNotFoundError:
        return "Nie znaleziono pliku publishers.json."
    return "Odczyt z pliku JSON zakonczony powodzeniem!"


def get_game_list(name):
    import models.game as games
    game_list = []
    for keys, values in games.game_list.items():
        if values["_publisher"] == name:
            game_list.append(values["_name"])
    return game_list
